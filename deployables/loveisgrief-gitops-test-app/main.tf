terraform {
  required_providers {
    local = {}
  }
}

resource "local_file" "foo" {
    content  = "${var.branch}!"
    filename = "/foo.txt"
}
