"""
gitops-test-deployment
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# Fuck using bash...
import argparse
import logging
from os import environ
from pathlib import Path

import slugify

from .dicts import get_dict_value
from .jobs import get_build_env, get_parent_info

REQUIRED_ENV_VARS = {
    "CI_COMMIT_SHA",
    "CI_PROJECT_PATH",
    "CI_SERVER_URL",
}
logger = logging.getLogger("deployer")

parser = argparse.ArgumentParser(
    description="Main script to trigger the deployment of apps that this repo can handle. "
                f"Required environment variables: {','.join(sorted(REQUIRED_ENV_VARS))}",
    prog="deployer"
)
parser.add_argument("token", help="Personal access token to use GraphQL API")
parser.add_argument(
    "--env-job",
    default="deploy_env",
    help="Name of job in parent pipeline that contains the env"
)
parser.add_argument(
    "--debug",
    action="store_true",
    help="Activate debug logs"
)

args = parser.parse_args()

# Ensure all required environment varas have been passed
if missing_env_vars := REQUIRED_ENV_VARS - set(environ.keys()):
    logger.error("Missing env vars: %s", ",".join(missing_env_vars))
    exit(1)

# configure logging
logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

parent_info = get_parent_info(environ["CI_PROJECT_PATH"], environ["CI_COMMIT_SHA"], args.token)

parent_project = get_dict_value(parent_info, "data.project.pipeline.sourceJob.pipeline.project.fullPath")
parent_commit_ref = get_dict_value(parent_info, "data.project.pipeline.sourceJob.pipeline.ref")
parent_jobs = get_dict_value(parent_info, "data.project.pipeline.sourceJob.pipeline.jobs.nodes")

directories = [path.name for path in Path("deployables").iterdir() if path.is_dir()]

# Make sure the parent project is deployable
if not parent_project:
    logger.error("Parent project not found in parent info: %s", parent_info)
    exit(1)

parent_project_slug = slugify.slugify(parent_project)
if parent_project_slug not in directories:
    logger.error("This repo cannot deploy parent project: %s", parent_project_slug)
    exit(1)

build_env = get_build_env(args.env_job, parent_jobs, environ["CI_SERVER_URL"])
logger.info("build env: %s", build_env)

logger.info("Can deploy %s", parent_project_slug)
logger.info("Will deploy ref %s", parent_commit_ref)

# TODO: Start deployment
