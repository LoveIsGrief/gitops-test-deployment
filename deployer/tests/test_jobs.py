import unittest

from deployer.jobs import parse_build_env


class ParseBuildEnvTests(unittest.TestCase):
    def test_empty(self):
        self.assertEqual(parse_build_env(""), {})

    def test_valid_string(self):
        self.assertEqual(
            parse_build_env(
                """
                        
                        BUILD_VERSION=test
                        some_var=another value
                        
                        another_var = with a space separated
                        
                        """
            ), {
                "BUILD_VERSION": "test",
                "some_var": "another value",
                "another_var": "with a space separated"
            }
        )

    def test_invalid_var_names(self):
        self.assertEqual(
            parse_build_env(
                """
                i n v a l i d name=no value
                ++++=true
                $%A=something
                ```=whatever
                æäíàé = invalid
                """
            ), {}
        )

    def test_invalid_syntax(self):
        self.assertEqual(
            parse_build_env(
                """

                =
                
                """
            ), {}
        )


if __name__ == '__main__':
    unittest.main()
