"""
gitops-test
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from typing import Any

from .errors import PathMissing, PathTooLong

FAILURE_DEFAULT = frozenset()


def get_dict_value(d: dict, path: str, default=FAILURE_DEFAULT) -> Any:
    current = d
    current_path = []
    for component in path.split("."):
        current_path.append(component)

        # We can only traverse dicts
        if not isinstance(current, dict):
            if default == FAILURE_DEFAULT:
                raise PathTooLong(path, ".".join(current_path), current)
            else:
                return default

        # We have to discern between the component existing and the value being None
        if component in current:
            current = current[component]
        elif default == FAILURE_DEFAULT:
            raise PathMissing(path, ".".join(current_path), current)
        else:
            return default
    return current
