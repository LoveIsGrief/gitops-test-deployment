"""
gitops-test
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


class PathError(ValueError):
    def __int__(self, complete_path, error_path, dictionary):
        self.complete_path = complete_path
        self.error_path = error_path
        self.dictionary_keys = list(dictionary.keys())


class PathMissing(PathError):
    """A component of the path is missing"""


class PathTooLong(PathError):
    """A component of the path is not a dict and one cannot resolve beyond that"""
