"""
gitops-test
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import gzip
import logging
import re
from typing import Dict, List, Optional

import requests

from deployer.dicts import get_dict_value

logger = logging.getLogger(__name__)


def get_build_env(job_name: str, jobs: List[dict], server_url: str) -> dict:
    deploy_env_job = find_job(job_name, jobs)
    if not deploy_env_job:
        logger.info("Parent job not found with name: %s", job_name)
        return {}

    url_path = get_build_env_url_path(deploy_env_job)
    if not url_path:
        logger.info("Build env vars not found in job: %s", job_name)
        return {}

    build_env_url = server_url + url_path
    build_env_response = requests.get(build_env_url)
    if build_env_response.status_code != 200:
        logger.error("Couldn't get build env from %s : %s", build_env_url, build_env_response.content)
        build_env_response.raise_for_status()

    build_env = gzip.decompress(build_env_response.content).decode()
    return parse_build_env(build_env)


def find_job(name: str, jobs: List[dict]) -> Optional[dict]:
    return next(filter(lambda job: job["name"] == name, jobs), None)


def get_build_env_url_path(job: dict) -> Optional[str]:
    artifacts = get_dict_value(job, "artifacts.nodes")
    return next(filter(lambda artifact: artifact["fileType"] == "DOTENV", artifacts), {}).get("downloadPath")


def parse_build_env(build_env: str) -> Dict[str, str]:
    """
    Accept newline delimited strings with [a-zA-Z0-9_] variables names separated by = followed by a string

    See tests for valid and invalid examples
    """
    env_vars = {}
    for line in build_env.split("\n"):
        line = line.strip()
        if not line:
            continue
        var_name, _, var_value = line.strip().rpartition("=")
        var_name = var_name.strip()
        var_value = var_value.strip()

        if not is_valid_var_name(var_name):
            logger.warning("var name is not good: %s", var_name)
            continue

        env_vars[var_name.strip()] = var_value.strip()
    return env_vars


def is_valid_var_name(var_name: str):
    return re.match(r'^\w+$', var_name, re.ASCII) is not None


def get_parent_info(project_path: str, sha: str, token: str) -> dict:
    """
    Get information about the parents of the running job

    :param project_path: The gitlab URL path to your project
    :param sha: As returned by CI_COMMIT_SHA
    :param token: Your personal access token to call the API
    :return: The GraphQL response
    """
    query = """
    query getParentInfo($projectPath: ID!, $pipelineSha: String) {
      project(fullPath: $projectPath) {
        pipeline(sha: $pipelineSha) {
          sourceJob {
            id
            name
            pipeline {
              id
              project {
                id
                fullPath
              }
              ref
              jobs{
                count
                nodes {
                  id
                  name
                  artifacts{
                    nodes{
                      name
                      fileType
                      downloadPath
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    """
    query_json = {
        "query": query,
        "variables": {
            "projectPath": f"{project_path}",
            "pipelineSha": f"{sha}"
        },
        "operation": "getParentInfo"
    }
    response = requests.post(
        "https://gitlab.com/api/graphql",
        json=query_json,
        headers={
            "Authorization": f"Bearer {token}"
        },
    )
    response.raise_for_status()
    return response.json()
