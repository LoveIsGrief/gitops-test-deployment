
# Requirements

 - Infrastructure As Code (Terraform)
 - Protected environments: only certain users can deploy
 - Push flow (pull flow is more complicated)
 - Secrets for deployment are in the application deployment repo (deployables repos)

# Structure

## Deployables

Applications that can be deployed are called "deployables".
They are in the `deployables` folder and are git submodules that point to app deployment repos.

Deployables contain the deployment instructions for projects.
In this case, terraform declarations (i.e Infrastructure as Code).
Each subfolder has to be the sluggified, lowercase name of the project triggering this repo.

**Examples**

| Project | Folder |
| ------- | ------ |
| LoveIsGrief/gitops-test-app | loveisgrief-gitops-test-app |
| NamingThingsIsHard/web/firefox/bifulushi | namingthingsishard-web-firefox-bifulushi |

This naming convention allows the folders to be unique.
One advantage is checking which repo triggered the pipeline to ensure it's deployable.

## Templates

Are in the `templates` folder.

These are used to build the IaC (Infrastructure as Code) in the deployables.
It simply works by copying `templates/base`, and (if it exists) the template folder with the same
 name as the deployable, into the deployable folder.


# Deployment

```mermaid
sequenceDiagram
    autonumber
    Developer ->> App repo: Push new version
    App repo ->> Developer: App can be deployed
    Deployer ->> App repo: Triggers a deployment
    App repo ->> Deploy repo: Trigger
    Deploy repo ->> Deploy repo: Check caller
    Deploy repo ->> Deploy repo: Render templates into app repo submodule
    Deploy repo ->> App deploy repo: Push new infrastructure
    Deploy repo ->> App deploy repo: Trigger pipeline
    App deploy repo ->> Secret provider(bitwarden): plz gib secrets
    Secret provider(bitwarden) ->> App deploy repo: u haz secrets
    App deploy repo ->> Cloud provider: Update state with terraform
```

## Environments

A `variables.auto.tfvars.json` is then written into the `<deployable>/<env>` folder by the CI
 which contains the variables necessary for terraform to deploy the application.

Determining which environment to deploy will be done by the name of the job triggering the deployment.
Jobs will simply to follow the format `Deploy <preview|test|prod>`.
Thanks to Gitlab's [protected environments] feature, one can restrict the right of deployment,
 and jobs can of course be automatically triggered under certain conditions [rules:if]
 e.g deploy to test after merging to `master`.

[rules:if]: https://docs.gitlab.com/ee/ci/yaml/#rulesif

# Security

**Permissions diagram**
```mermaid
erDiagram
          Developer }|--|| App-Repo : "can push"
          Deployer }|--|| App-Repo : "can deploy"
          App-Repo }|--|| Deployment-repo : "can trigger pipeline"
          DevOps }|--|| Deployment-repo: "can write"
          Deployment-repo ||--|{ App-Deployment-Repo : "can write"
          Deployment-repo ||--|{ App-Deployment-Repo : "can trigger pipeline"
          App-Deployment-Repo }|--|| Bitwarden : "can access secrets"
          App-Deployment-Repo }|--|| Cloud-Provider : "can update infra"

```

**Secrets diagram**

```mermaid
erDiagram
    Deployment-repo ||--|{ App-Deployment-Token : has
    App-Deployment-Token ||--|| App-Deployment-Repo: "gives write access"
    App-Deployment-Token ||--|| App-Deployment-Repo: "gives write access"
    App-Deployment-Repo }|--|| Bitwarden-Credentials : has
    Bitwarden-Credentials ||--|| Bitwarden: "gives read access"
    Bitwarden ||--|| Cloud-Provider-Secrets: has
    Cloud-Provider-Secrets ||--|| Cloud-Provider: "gives write access"
```



The developer only has access to the app repo.

Only a deployer can initiate the deployment of an app ([protected environments]).

The deployment repo's pipeline can only be triggered by apps it can deploy.
Their slugified path has to be in the `/deployables` folder.

App deployment repos can only modified with the [project access token] or a non-user access token. 
The deployment repo holds the access tokens to write to all the app deployment repos.

Deployment secrets are kept on a secret provider to which only the app deployment repo has the secrets to.
In this case it'll be bitwarden and the app deployment repo will only have access to one [collection][bitwarden collection].

[bitwarden collection]: https://bitwarden.com/help/about-collections/
[project access token]: https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html
[protected environments]: https://docs.gitlab.com/ee/ci/environments/protected_environments.html
